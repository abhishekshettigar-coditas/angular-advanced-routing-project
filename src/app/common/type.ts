export interface Iuser {
  name: string;
  username: string;
  email: string;
  id: number;
  phone: string;
  website: string;
}

export interface Icompany {
  name: string;
  catchPhrase: string;
  bs: string;
}
