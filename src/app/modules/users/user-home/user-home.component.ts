import { Component, OnInit } from '@angular/core';
import { Iuser } from 'src/app/common/type';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-user-home',
  templateUrl: './user-home.component.html',
  styleUrls: ['./user-home.component.scss'],
})
export class UserHomeComponent implements OnInit {
  users: Iuser[] = [];

  constructor(private http: DataService) {}

  ngOnInit(): void {
    this.http.getAllUsers().subscribe({
      next: (response: any) => (this.users = response),
    });
  }
}
