import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Icompany, Iuser } from 'src/app/common/type';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-company-details',
  templateUrl: './company-details.component.html',
  styleUrls: ['./company-details.component.scss'],
})
export class CompanyDetailsComponent implements OnInit {
  selectedId: number = 0;
  selectedUserCompany!: Icompany;
  constructor(private route: ActivatedRoute, private http: DataService) {}
  ngOnInit(): void {
    this.selectedId = this.route.snapshot.params['id'];
    console.log(this.selectedId);
    this.route.params.subscribe((params: Params) => {
      this.selectedId = Number.parseInt(params['id']);
    });
    this.http.getAllUsers().subscribe({
      next: (response: any) => {
        for (let user of response) {
          if (user.id === this.selectedId) {
            this.selectedUserCompany = user.company;
            console.log(this.selectedUserCompany, 'sheesh');
          }
        }
      },
    });
  }
}
