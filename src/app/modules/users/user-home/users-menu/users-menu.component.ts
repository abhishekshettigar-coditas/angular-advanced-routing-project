import { Component, Input, OnInit } from '@angular/core';
import { Iuser } from 'src/app/common/type';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-users-menu',
  templateUrl: './users-menu.component.html',
  styleUrls: ['./users-menu.component.scss'],
})
export class UsersMenuComponent implements OnInit {
  @Input('users') users: Iuser[] = [];
  constructor(private data: DataService) {}

  ngOnInit(): void {}
}
