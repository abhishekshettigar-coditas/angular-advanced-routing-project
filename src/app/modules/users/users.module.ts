import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsersRoutingModule } from './users-routing.module';
import { UserHomeComponent } from './user-home/user-home.component';
import { UsersMenuComponent } from './user-home/users-menu/users-menu.component';
import { UserDetailsComponent } from './user-home/user-content/user-details/user-details.component';
import { UserContentComponent } from './user-home/user-content/user-content.component';
import { CompanyDetailsComponent } from './user-home/user-content/company-details/company-details.component';

@NgModule({
  declarations: [
    UserHomeComponent,
    UsersMenuComponent,
    UserDetailsComponent,
    UserContentComponent,
    CompanyDetailsComponent,
  ],
  imports: [CommonModule, UsersRoutingModule],
})
export class UsersModule {}
